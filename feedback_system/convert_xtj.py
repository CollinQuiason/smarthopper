#Collin Rottinghaus
#SmartKC
#Convert From XML to JSON format
#(XML -> Dictionary -> String -> JSON)


import json
import xmltodict
 
def partition(array, begin, end):
    pivot = begin
    for i in xrange(begin+1, end+1):
        if array[i] <= array[begin]:
            pivot += 1
            array[i], array[pivot] = array[pivot], array[i]
    array[pivot], array[begin] = array[begin], array[pivot]
    return pivot



def quicksort(array, begin=0, end=None):
    if end is None:
        end = len(array) - 1
    def _quicksort(array, begin, end):
        if begin >= end:
            return
        pivot = partition(array, begin, end)
        _quicksort(array, begin, pivot-1)
        _quicksort(array, pivot+1, end)
    return _quicksort(array, begin, end)




def convert(xml_file, veh = False, xml_attribs=True):
	with open(xml_file, "rb") as infile, open(xml_file[:-4] + '.json', 'w') as outfile:    # notice the "rb" mode
		dic = xmltodict.parse(infile, xml_attribs=xml_attribs)
#		if (veh):
#			for i in dic['vehicles']:
#				theKeys = i.keys()
#				for q in theKeys:
#					if (q != 'co2TailpipeGpm' and q != 'make' and q != 'model' and q != 'year' and q != 'highway08'):
#						i.pop(q)

		outfile.write(json.dumps(dic, indent=4))




convert("vehicles.xml", True)
convert("emissions.xml")
convert("fuelPrices.xml")






