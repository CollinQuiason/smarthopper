    /*
 *  Licensed to GraphHopper GmbH under one or more contributor
 *  license agreements. See the NOTICE file distributed with this work for
 *  additional information regarding copyright ownership.
 *
 *  GraphHopper GmbH licenses this file to you under the Apache License,
 *  Version 2.0 (the "License"); you may not use this file except in
 *  compliance with the License. You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.smartresident;

import com.graphhopper.reader.osm.OSMReader;
import com.graphhopper.routing.util.FlagEncoder;
import com.graphhopper.routing.util.HintsMap;
import com.graphhopper.routing.weighting.AbstractWeighting;
import com.graphhopper.util.EdgeIteratorState;
import com.graphhopper.util.PMap;
import com.graphhopper.util.Parameters.Routing;
import org.joda.time.LocalDateTime;
import org.joda.time.LocalDateTime.Property;
import java.util.Date;
import java.util.Calendar;
import java.util.GregorianCalendar;
import com.graphhopper.util.CmdArgs;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
    public class TrafficWeighting extends AbstractWeighting {
        /**
         * Converting to seconds is not necessary but makes adding other penalties easier (e.g. turn
         * costs or traffic light costs etc)
         */
        protected final static double SPEED_CONV = 3.6;
        //private final double trafficcPenalty;
        //private final long trafficPenaltyMillis;
        private final double maxSpeed;

        public TrafficWeighting(FlagEncoder encoder, PMap map) {
            super(encoder);
            maxSpeed = encoder.getMaxSpeed() / SPEED_CONV;
        }

        public TrafficWeighting(FlagEncoder encoder) {
            this(encoder, new HintsMap(0));
        }

        @Override
        public double getMinWeight(double distance) {
            return distance / maxSpeed;
        }

        @Override
        public double calcWeight(EdgeIteratorState edge, boolean reverse, int prevOrNextEdgeId) {
            double speed = reverse ? flagEncoder.getReverseSpeed(edge.getFlags()) : flagEncoder.getSpeed(edge.getFlags());
            if (speed == 0)
                return Double.POSITIVE_INFINITY;
            LocalDateTime dt = new LocalDateTime().now();
            int hour = dt.getHourOfDay();
            int minute = dt.getMinuteOfHour();
            if (minute > 29)
                hour = hour + 1;
            //Get street name from edge. Make call to get traffic value from API for that edge
            //convert that value to int, divide by 10 to turn into a percentage of speed, multiply trafficValue with speed to get
            //new speed value after traffic is accounted for.

            String trafficReturn = AltMap.getData(edge.getName(), "traffic", hour);

            int trafficValue = Integer.parseInt(trafficReturn);
            trafficValue = trafficValue/10;
            double time = edge.getDistance() / speed * SPEED_CONV * trafficValue;

            return time;
        }

        @Override
        public String getName() {
            return "fastestwithtraffic";
        }

    }

