package com.smartresident;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;
import java.io.File;
import java.io.PrintWriter;
import java.util.Collections;
import java.util.Random;
import java.util.Vector;

public class AltMap {
    private static String filePath = new File("").getAbsolutePath();
/*
    public static void main(String args[]) //   ****Example of usage****
    {
        generate(); //CREATES "traffic.xml"
        System.out.println(getData("Mark Twain Apartments", "traffic", 6)); //Outputs specified data
    }
*/
    //Wrapper function
    private static int binarySearch(NodeList arr, String street)
    {
        return binarySearch(arr, 0, arr.getLength(), street);
    }
    //Logarithmic search of elements's tag in nodelist, returns index of element
    private static int binarySearch(NodeList arr, int l, int r, String fs)
    {
        if (r >= l)
        {
            int mid = l + (r - l)/2;
            Element nElement = (Element)arr.item(mid);
            String street_name = (nElement.getAttribute("ref"));
            int comparison = street_name.compareTo(fs);
            // If the element is present at the middle
            if (comparison == 0) return mid;

            // If element is smaller than mid, then it can only be present
            // in left subarray
            if (comparison > 0) return binarySearch(arr, l, mid-1, fs);

            // Else the element can only be present in right subarray
            return binarySearch(arr, mid+1, r, fs);
        }

        // We reach here when element is not present in array
        return -1;
    }

    public static String getData(String nodeID, String tag, int hour)//tag examples: traffic/weather/incidents etc, hours should be 0-23
    {
        try {
            //Importing XML to document
            File fXmlFile = new File(filePath + "/map/traffic.xml");
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(fXmlFile);
            doc.getDocumentElement().normalize();

            //Data
            NodeList nodes = doc.getElementsByTagName("nd");
            int index = binarySearch(nodes, nodeID);
            if (index == -1) //Node ID does not exist
            {
                return "9";
            }
            Element nElement = (Element)nodes.item(index);
            NodeList hours = nElement.getElementsByTagName(tag);

            return hours.item(hour).getTextContent();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return "error in getData()";
    }
    public static void generate()
    {
        try {
            //Importing XML to document
            File fXmlFile = new File(filePath + "/map/umkc.osm");
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(fXmlFile);
            doc.getDocumentElement().normalize();

            //Data
            NodeList nodes = doc.getElementsByTagName("way");
            Vector<String> streetVector = new Vector<String>();
            int[][] trafVector = new int[nodes.getLength()][24];
            Random rand = new Random();

            //Traffic rng
            for (int i = 0; i < nodes.getLength(); i++)
            {
                Node singleNode = nodes.item(i);
                if (singleNode.getNodeType() == Node.ELEMENT_NODE)
                {
                    Element nElement = (Element) singleNode;
                    NodeList wayTags = nElement.getElementsByTagName("tag");
                    for (int e = 0; e < wayTags.getLength(); e++)
                    {
                        String street_name = "null";
                        Element tag = (Element)wayTags.item(e);
                        if (tag.getAttribute("k").equals("name")) {
                            street_name = (tag.getAttribute("v"));
                            if (!streetVector.contains(street_name)) {
                                streetVector.add(street_name);
                            }
                        }
                    }
                }

                for (int q = 0; q < 24; q++)
                {
                    trafVector[i][q] = (rand.nextInt(10) + 1);
                }
            }

            Collections.sort(streetVector);

            for (int i = 0; i < streetVector.size(); i++) //DOMParser doesn't work with "&", they must be converted to "&amp"
            {
                String temp = streetVector.get(i);
                streetVector.set(i,temp.replaceAll("&", "&amp;"));
            }



            //Output to XML file
            PrintWriter out = new PrintWriter(filePath + "/map/traffic.xml");

            out.println("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
            out.println("<osm>");
            for (int i = 0; i < streetVector.size(); i++)
            {
                out.println("<nd ref=\"" + streetVector.get(i) + "\">");
                for (int q = 0; q < 24; q++)
                {
                    out.println("   <traffic>"+trafVector[i][q] + "</traffic>");
                }
                out.println("</nd>");
            }
            out.println("</osm>");

            out.close();
            System.out.println("Created \"Traffic.xml\" ...");
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Exception thrown in generate()");
        }
    }
    /* Possible static NodeList instead of being initialized in every function
    private static NodeList nodes;
    private static void insertNodes
    {
    }
    */

}



