package com.smartresident;

import com.graphhopper.routing.util.EncodingManager;
import java.io.File;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import com.graphhopper.GHRequest;
import com.graphhopper.GHResponse;
import com.graphhopper.PathWrapper;
import com.graphhopper.util.*;
import org.joda.time.LocalDateTime;
//TODO: Include traffic information in paths.json, path1.json -> path3.json
//TODO: Have all path.json information be output through one function
//TODO: Reformat Graphhopper's ".createJson()" to use quotations
public class App 
{
    public static void main( String[] args )
    {
        AltMap.generate(); //Generates traffic data
        // create one GraphHopper instance
        String filePath = new File("").getAbsolutePath();
        MyGraphHopper hopper = new MyGraphHopper();
        hopper.setDataReaderFile(filePath + "/map/umkc_01.pbf");
        // where to store graphhopper files?
        hopper.setGraphHopperLocation(filePath + "/map");
        hopper.setEncodingManager(new EncodingManager("car"));

        // now this can take minutes if it imports or a few seconds for loading
        // of course this is dependent on the area you import
        hopper.importOrLoad();
        hopper.getCHFactoryDecorator().setDisablingAllowed(true);
        // simple configuration of the request object, see the GraphHopperServlet class for more possibilities.
        GHRequest req = new GHRequest(39.0350, -94.5920,39.0330, -94.5794).

                setWeighting("fastestwithtraffic"). //Use for switching to traffic weighting
                //setWeighting("fastest").
                setVehicle("car").
                setLocale(Locale.US);
        req.setAlgorithm(Parameters.Algorithms.DIJKSTRA);
        req.getHints().put("ch.disable", true);
        GHResponse rsp = hopper.route(req);

        // first check for errors
        if(rsp.hasErrors()) {
            // handle them!
            // rsp.getErrors()
            return;
        }

        // use the best path, see the GHResponse class for more possibilities.
        PathWrapper path = rsp.getBest();

        // points, distance in meters and time in millis of the full path
        PointList pointList = path.getPoints();
        double distance = path.getDistance();
        long timeInMs = path.getTime();

        InstructionList il = path.getInstructions();
        // iterate over every turn instruction

        for(Instruction instruction : il) {
            instruction.getDistance();
        }

        // or get the json
        List<Map<String, Object>> iList = il.createJson();
        try{
            PrintWriter path1 = new PrintWriter(filePath + "/path1.json");
            path1.print(iList.toString());

            path1.close();
            System.out.println("Created \"path1.json\" ...");
        }catch(Exception e){
            System.out.println("Failed to create \"path1.json\" ...");
            e.printStackTrace();
        }

        ////////////////////////////////////////////////////////////

        ////////////////////////////////////////////////////////////




        LocalDateTime now = new LocalDateTime();
        Date date = new Date();
        String dateFormat = "hh:mm:ss-aa";
        SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
        String pathsOutput = "";
        pathsOutput = pathsOutput.concat("{\n");
        pathsOutput = pathsOutput.concat("\"time\":\""+ sdf.format(date) +"\",\n");
        pathsOutput = pathsOutput.concat("\"weather\":\""+ "weather" + "\",\n");
        pathsOutput = pathsOutput.concat("\"date\":\""+ now.getMonthOfYear() + "/" + now.getDayOfMonth() + "/" + now.getYearOfCentury() + "\",\n");
        pathsOutput = pathsOutput.concat("\"paths\": [\n");
        pathsOutput = pathsOutput.concat("   {\n");
        pathsOutput = pathsOutput.concat("   \"nPath\": \"1\",\n");
        pathsOutput = pathsOutput.concat("   \"distance\": \""+ distance +"\",\n");
        pathsOutput = pathsOutput.concat("   \"time\": \""+ timeInMs +"\",\n");
        pathsOutput = pathsOutput.concat("   \"traffic\": \""+ 1 +"\"\n");
        pathsOutput = pathsOutput.concat("   },\n");





            System.out.println("Created \"paths.json\" part 1...");


        /////////////////////////////////////////////////////////
        req.setAlgorithm(Parameters.Algorithms.DIJKSTRA_BI);
        req.getHints().put("ch.disable", true);
        rsp = hopper.route(req);

        // first check for errors
        if(rsp.hasErrors()) {
            // handle them!
            // rsp.getErrors()
            return;
        }

        // use the best path, see the GHResponse class for more possibilities.
        path = rsp.getBest();

        // points, distance in meters and time in millis of the full path
        pointList = path.getPoints();
        distance = path.getDistance();
        timeInMs = path.getTime();

        il = path.getInstructions();
        // iterate over every turn instruction
        for(Instruction instruction : il) {
            instruction.getDistance();
        }

        // or get the json
        iList = il.createJson();
        try{
            PrintWriter path1 = new PrintWriter(filePath + "/path2.json");
            path1.print(iList.toString());

            path1.close();
            System.out.println("Created \"path2.json\" ...");
        }catch(Exception e){
            System.out.println("Failed to create \"path2.json\" ...");
            e.printStackTrace();
        }


        pathsOutput = pathsOutput.concat("   {\n");
        pathsOutput = pathsOutput.concat("   \"nPath\": \"2\",\n");
        pathsOutput = pathsOutput.concat("   \"distance\": \""+ distance +"\",\n");
        pathsOutput = pathsOutput.concat("   \"time\": \""+ timeInMs +"\",\n");
        pathsOutput = pathsOutput.concat("   \"traffic\": \""+ 1 +"\"\n");
        pathsOutput = pathsOutput.concat("   },\n");




            System.out.println("Created \"paths.json\" part 2...");


        ///////////////////////////////////////////////////////////
        req.setAlgorithm(Parameters.Algorithms.ASTAR);
        req.getHints().put("ch.disable", true);
        rsp = hopper.route(req);

        // first check for errors
        if(rsp.hasErrors()) {
            // handle them!
            // rsp.getErrors()
            return;
        }

        // use the best path, see the GHResponse class for more possibilities.
        path = rsp.getBest();

        // points, distance in meters and time in millis of the full path
        pointList = path.getPoints();
        distance = path.getDistance();
        timeInMs = path.getTime();

        il = path.getInstructions();
        // iterate over every turn instruction
        for(Instruction instruction : il) {
            instruction.getDistance();
        }

        // or get the json
        iList = il.createJson();
        try{
            PrintWriter path1 = new PrintWriter(filePath + "/path3.json");
            path1.print(iList.toString());

            path1.close();
            System.out.println("Created \"path3.json\" ...");
        }catch(Exception e){
            System.out.println("Failed to create \"path3.json\" ...");
            e.printStackTrace();
        }




        pathsOutput = pathsOutput.concat("   {\n");
        pathsOutput = pathsOutput.concat("   \"nPath\": \"3\",\n");
        pathsOutput = pathsOutput.concat("   \"distance\": \""+ distance +"\",\n");
        pathsOutput = pathsOutput.concat("   \"time\": \""+ timeInMs +"\",\n");
        pathsOutput = pathsOutput.concat("   \"traffic\": \""+ 1 +"\"\n");
        pathsOutput = pathsOutput.concat("   }\n");
        pathsOutput = pathsOutput.concat("]}\n");
        System.out.println("Created \"paths.json\" part 3...");
        try{
            PrintWriter paths = new PrintWriter(filePath+"/paths.json");
            paths.println(pathsOutput);
            paths.close();
            System.out.println("Finished writing \"paths.json\"...");
        }catch(Exception e){
            e.printStackTrace();
        }


        // or get the result as gpx entries:
        //        List<GPXEntry> list = il.createGPXList();
    }
}
