package com.smartresident;


import com.graphhopper.GraphHopper;
import com.graphhopper.routing.util.FlagEncoder;
import com.graphhopper.routing.util.HintsMap;
import com.graphhopper.routing.weighting.Weighting;
import com.graphhopper.storage.Graph;

class MyGraphHopper extends GraphHopper {
    @Override
    public Weighting createWeighting(HintsMap hintsMap, FlagEncoder encoder, Graph graph) {
        String weightingStr = hintsMap.getWeighting().toLowerCase();

        if ("fastestwithtraffic".equals(weightingStr)) {
            return new TrafficWeighting(encoder);
        } else {
            return super.createWeighting(hintsMap, encoder, graph);
        }
    }
}
