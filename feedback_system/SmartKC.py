#Collin Rottinghaus
#SmartKC
#Process data and output in json format


import json
import os


#File Initialization (JSON)
path = os.getcwd()

VJSON = open(path + "\\vehicles.json")
VOBJ = VJSON.read()
vehicles = json.loads(VOBJ)

EJSON = open(path + "\\emissions.json")
EOBJ = EJSON.read()
emissions = json.loads(EOBJ)

FJSON = open(path + "\\fuelPrices.json")
FOBJ = FJSON.read()
fuelPrices = json.loads(FOBJ)

DJSON = open(path + "\\data.json")
DOBJ = DJSON.read()
data = json.loads(DOBJ)


#aggregation of JSON file information
distance = 0
time = 0
for i in data:
	distance += int(i['distance'])
	time += int(i['time'])


#Functions
def findVehicle(ma, mo, ye):
	for i in vehicles['vehicles']['vehicle']:
		if i['make'] == ma and i['model'] == mo and i['year'] == ye:
			return i
	return vehicles['vehicles']['vehicle'][0]
def grams(i, miles):
	return (float(i['co2TailpipeGpm'])*miles)
def gramsEmitted(i, miles):
	return str(grams(i, miles)/1000) + " Kilograms of CO2 were emitted during the " + str(miles) + " mile trip."
def treesRequired(grams, seconds):
	return grams/(COTWOPERSEC * seconds)
def gasUsed(i, miles):
	return miles/float(i['highway08'])
def moneySpent(gallons):
	return gallons * float(fuelPrices["fuelPrices"]["regular"])
def moneyDifference(c1, c2, miles):
	return moneySpent(gasUsed(c1, miles)) - moneySpent(gasUsed(c2, miles))
def outputData(veh, mi, sec):
	print(str(gasUsed(veh, mi)) + " Gallons Used")
	print("$" + str(moneySpent(gasUsed(veh, mi))) + " Spent")
	print(gramsEmitted(veh, mi))
	print(str(treesRequired(grams(veh, mi), sec)) + " Trees required required to neutralize CO2")


#Data
car1 = findVehicle(data["make"], data["model"], data["year"])
car2 = findVehicle("Toyota", "Prius", "2012")
COTWOPERSEC = 0.0006903993386 #Grams per second removed by an "average" tree
MILES = distance #TODO - convert units
SECONDS = time #TODO - convert units

#Output to "output.json"
output = {
	'emissionsKG': str(grams(car1, MILES)),
	'moneySpent': str(moneySpent(gasUsed(car1, MILES))),
	'moneyDifference': str(moneyDifference(car1, car2, MILES)),
	'trees': str(treesRequired(grams(car1, MILES), SECONDS)),
	'gasUsed': str(gasUsed(car1, MILES))
}

with open("output.json", 'w') as out:
	out.write(json.dumps(output, indent = 4))


#Output to console
#print("Mazda Miata 2000")
#outputData(car1, MILES, SECONDS)
#print()
#print("Toyota Prius 2012")
#outputData(car2, MILES, SECONDS)
#print()
#print("$" + str(moneyDifference(car1, car2, MILES)) + " Money saved when using Prius")


#findVehicle("Mazda", "MX-5 Miata", "2000")
#for i in vehicles['vehicles']['vehicle']:
#	print(i['make'], end = '	')
#	print(i['model'], end = '	')
#	print(i['year'], end = '	')
#	print("City MPG:", end = '')
#	print(i['UCity'], end = '	')
#	print("Highway MPG:", end = '')
#	print(i['UHighway'])






